var app = angular.module('visualizator', ['ui.bootstrap']);

app.controller('jsonController', function($scope, $http) {
	$scope.sections = [];			// array to store all sections
	$scope.symbolsElements = [];	// array to store all symbols
	
	$scope.currentSectionPage = 1; // keeps track of the sections current page
	$scope.sortSectionKey;
	$scope.reverseSection;
	
	$scope.currentSymbolsPage = 1; // keeps track of the symbols current page
	$scope.sortSymbolKey
	$scope.reverseSymbol;
	
	$scope.pageSize = 10; 	// holds the number of items per page
	
	$http.get("http://localhost:8080/esr/elf.json").then(function(response) {
		$scope.sections = Object.keys(response.data.sections).map(function(k) { return response.data.sections[k] });
		$scope.symbolsElements = Object.keys(response.data.symbols.elements).map(function(k) { return response.data.symbols.elements[k] });
	});

	// Sort the sections table
	$scope.sortSection = function(keyname){
		$scope.sortSectionKey = keyname;   					//set the sortKey to the param passed
		$scope.reverseSection = !$scope.reverseSection; 	//if true make it false and vice versa
	};
	
	// Sort the symbols table
	$scope.sortSymbol = function(keyname){
		$scope.sortSymbolKey = keyname;   				//set the sortKey to the param passed
		$scope.reverseSymbol = !$scope.reverseSymbol; 	//if true make it false and vice versa
	};
})
	// Apply the filter on the table
.filter('start', function () {
	return function (input, start) {
		if (!input || !input.length) { return; }

		start = +start;
		return input.slice(start);
	};
});