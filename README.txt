EST Test Case

This exercise was built using AngularJS and Bootstrap. It is composed by two separated tables for the sectors and symbols. You can
reorder, search items and use pagination on the table.

How to use:
1. Download the Tomcat
2. Copy files for the Tomcat webapps folder.
3. Start tomcat.
4. Access the visualizator.html page. (localhost:8080/esr/visualizator.html)